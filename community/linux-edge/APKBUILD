# Maintainer: Milan P. Stanić <mps@arvanta.net>

_flavor=edge
pkgname=linux-${_flavor}
# NOTE: this kernel is intended for testing
# please resist urge to upgrade it blindly
pkgver=6.10.6
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux latest stable kernel"
url="https://www.kernel.org"
depends="initramfs-generator"
_depends_dev="perl gmp-dev elfutils-dev flex bison"
makedepends="$_depends_dev
	bc
	diffutils
	findutils
	installkernel
	linux-headers
	linux-firmware-any
	openssl-dev
	python3
	sed
	xz"

options="!strip !check" # no tests
_config=${config:-config-edge.${CARCH}}

subpackages="$pkgname-dev:_dev:$CBUILD_ARCH $pkgname-doc:_doc"
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz"
case $pkgver in
	*.*.0)	source="$source";;
	*.*.*)	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz" ;;
esac

source="$source
	config-edge.aarch64
	config-edge.armv7
	config-edge.x86_64
	config-edge.riscv64
	"

builddir="$srcdir/linux-${_kernver}"
arch="armv7 aarch64 x86_64 riscv64"
license="GPL-2.0"

_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors ${_f}"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-${_f}::$CBUILD_ARCH linux-${_f}-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
riscv64) _carch="riscv" ;;
esac

prepare() {
	local _patch_failed=
	cd $builddir
	case $pkgver in
		*.*.0);;
		*)
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N ;;
	esac

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.${CARCH}
		mkdir -p "$builddir"
		echo "-$pkgrel-$i" > "$builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$builddir"/.config
		make -C $builddir \
			O="$builddir" \
			ARCH="$_carch" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$builddir"
		make ARCH="$_carch" DTC_FLAGS="-@" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=${pkgver}-${pkgrel}-${_buildflavor}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$builddir"
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		riscv64) _install="install dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/${_abi_release}/build \
		"$_outdir"/lib/modules/${_abi_release}/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package edge "$pkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=${pkgver}-${pkgrel}-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-${_abi_release}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.${CARCH} "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine
	cd $builddir

	echo "Installing headers..."
	case "$_carch" in
	x86_64)
		_carch="x86"
		install -Dt "${dir}/tools/objtool" $builddir/tools/objtool/objtool
		;;
	esac
	cp -t "$dir" -a $builddir/include

	install -Dt "${dir}" -m644 $builddir/Makefile
	install -Dt "${dir}" -m644 $builddir/Module.symvers
	install -Dt "${dir}" -m644 $builddir/System.map
	cp -t "$dir" -a $builddir/scripts

	install -Dt "${dir}/arch/${_carch}" -m644 $builddir/arch/${_carch}/Makefile
	install -Dt "${dir}/arch/${_carch}/kernel" -m644 $builddir/arch/${_carch}/kernel/asm-offsets.s
	cp -t "${dir}/arch/${_carch}" -a $builddir/arch/${_carch}/include

	install -Dt "$dir/drivers/md" -m644 drivers/md/*.h
	install -Dt "$dir/net/mac80211" -m644 net/mac80211/*.h

	# https://bugs.archlinux.org/task/13146
	install -Dt "$dir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

	# https://bugs.archlinux.org/task/20402
	install -Dt "$dir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
	install -Dt "$dir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
	install -Dt "$dir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

	# https://bugs.archlinux.org/task/71392
	install -Dt "$dir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

	echo "Installing KConfig files..."
	find . -name 'Kconfig*' -exec install -Dm644 {} "$dir/{}" \;

	echo "Removing unneeded architectures..."
	local arch
	for arch in "$dir"/arch/*/; do
		case $(basename "$arch") in $_carch) continue ;; esac
		echo "Removing $(basename "$arch")"
		rm -r "$arch"
	done

	echo "Removing broken symlinks..."
	find -L "$builddir" -type l -printf 'Removing %P\n' -delete

	echo "Removing loose objects..."
	find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

	echo "Stripping build tools..."
	local file
	while read -rd '' file; do
		case "$(file -bi "$file")" in
			application/x-sharedlib\;*)      # Libraries (.so)
				strip -v $STRIP_SHARED "$file" ;;
			application/x-archive\;*)        # Libraries (.a)
				strip -v $STRIP_STATIC "$file" ;;
			application/x-executable\;*)     # Binaries
				strip -v $STRIP_BINARIES "$file" ;;
			application/x-pie-executable\;*) # Relocatable binaries
				strip -v $STRIP_SHARED "$file" ;;
		esac
	done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

	echo "Stripping vmlinux..."
	strip -v $STRIP_STATIC "$builddir/vmlinux"

	echo "Adding symlink..."
	mkdir -p "$subpkgdir"/lib/modules/${_abi_release}
	ln -sf /usr/src/linux-headers-${_abi_release} \
		"$subpkgdir"/lib/modules/${_abi_release}/build
}

_doc() {
	pkgdesc="documentation for $_flavor kernel"
	mkdir -p "$subpkgdir"/usr/share/doc/linux-edge-doc
	cp -r "$builddir"/Documentation \
		"$subpkgdir"/usr/share/doc/linux-edge-doc/

}

sha512sums="
baa2487954044f991d2ae254d77d14a1f0185dd62c9f0fcaff69f586c9f906823017b8db1c4588f27b076dfa3ebb606929fec859f60ea419e7974330b9289cc2  linux-6.10.tar.xz
cbff973f059aab36a8df5f71d1230a27fa24d7a0791dbe5249618beca37a7964d38c3dd9b544484e4ea2aa38c188dcd913678a9c05d92b5f5d4d34d954e0da1d  patch-6.10.6.xz
73cc54143d5d171de00492fada1456c5ade597b8780961211fd0dcc02ad906a65ce781329718c4f936670d853dbf55115230c72fe5b737aaf747f61f7007e63a  config-edge.aarch64
167ec98d1780b5ec7a807f0aa00e3d63961cb9937a9d89b5813e389086510665b2adf300255ec6aa2a367feda2e94df61aacfee450660b2df9326595c2266112  config-edge.armv7
740639ad3340801f486f6521bd79e0150a242de172440a765b65d659f4302f98f01058878cdff3f4fddaa5de4b524289b169b8e1f63b2eba5911a356fb85dbe7  config-edge.x86_64
327f9337b367ce735b12f378eedea19f22d3dd96bfe98e27f99aefaaee9af828d9af83d85838acfc9a34573c4e07623ad6ed8e7e046d5c65214aa6e887c712d8  config-edge.riscv64
"
