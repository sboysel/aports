# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=py3-qbittorrent-api
pkgver=2024.8.65
pkgrel=0
pkgdesc="Python client implementation for qBittorrent's Web API"
url="https://github.com/rmartin16/qbittorrent-api"
arch="noarch"
license="MIT"
depends="
	py3-requests
	py3-setuptools
	"
makedepends="
	py3-gpep517
	py3-wheel
	py3-setuptools_scm
	py3-sphinx-copybutton
	py3-sphinx-autodoc-typehints
	"
subpackages="$pkgname-doc $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rmartin16/qbittorrent-api/archive/v$pkgver.tar.gz"
builddir="$srcdir/qbittorrent-api-$pkgver"
# tests requires an instance of qbittorrent running
# net for sphinx
options="net !check"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
	sphinx-build -b man docs/source docs/build
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	install -Dm644 docs/build/qbittorrent-api.1 \
		-t "$pkgdir"/usr/share/man/man1

	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/tests
}

sha512sums="
ee3bd2b4d1579322ab8191b366474b466765f219e9e7fa0a090010b4b318baef756f083a53839a0f89389e53c453895a90a9de854d9a738f053fb7aa8ff02f1c  py3-qbittorrent-api-2024.8.65.tar.gz
"
